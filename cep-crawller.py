from urllib.parse import urlencode
from urllib.request import Request, urlopen


def unescapeXml(s):
    s = s.replace("&lt;", "<")
    s = s.replace("&gt;", ">")
    s = s.replace("&amp;", "&")
    s = s.replace("&nbsp;", " ")
    return s

def unescapeString(s):
    s = s.replace('\\r', '')
    s = s.replace('\\t', '')
    s = s.replace('\\n', '')
    return s

def getStringPosicao(result, find1, find2):
    posicao1 = int(result.index(find1) + len(find1))
    posicao2 = int(result.index(find2))
    texto = result[posicao1 : posicao2]
    return texto
    
def tirarInicio(result, find1):
    posicao1 = int(result.index(find1) + len(find1))
    texto = result[posicao1 : ]
    return texto

def processaResult(result):
    texto = getStringPosicao(result, "CEP:</th>", "</table>")
    logradouro = getStringPosicao(texto, 'width="150">', "</td>")
    texto = tirarInicio(texto, "</td>")
    setor = getStringPosicao(texto, '<td>', "</td>")
    texto = tirarInicio(texto, "</td>")
    cidade = getStringPosicao(texto, '<td>', "</td>")
    cep = {'logradouro': logradouro, 'setor': setor, 'cidade': cidade}
    return cep


parametros = {"relaxation": '74290140', "tipoCEP": "ALL", "semelhante": "N"}

url = "http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm"

req =  Request(url, urlencode(parametros).encode())
result = urlopen(req).read()
result = str(result)

result = unescapeString(result)
result = bytes(result, "iso-8859-1").decode("unicode_escape")
result = unescapeXml(result)




print(result)

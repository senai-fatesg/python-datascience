## Desafio 1 (aula de sábado)
https://www.urionlinejudge.com.br/judge/pt/problems/view/1049

## Atividade avaliativa

### Entregar:

1. notebook com exemplo de decorator

1. resolução do desafio:

escrever um bot no telegram que lê /temp e retorna a temperatura a partir de um site de clima de uma cidade específica, cria um arquivo json com esta solicitação e grava no disco.

```bash
Grupos de até 4 pessoas
Data da entrega: 23/09/2019
Entregar via email: atividades@franciscocalaca.com
Colocar nome dos integrantes no corpo do email
```
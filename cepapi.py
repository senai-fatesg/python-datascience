# pip install flask
from flask import Flask, render_template, request
import ceplib as ceplib
import json

app = Flask(__name__)

@app.route("/cep", methods = ['GET'])	
def getCep():
    cepcons = request.args.get('cep')
    cep = ceplib.consultaCep(cepcons)
    return json.dumps(cep, ensure_ascii=False); 


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True,port=8080)    

#http://localhost:8080/cep?cep=74290140